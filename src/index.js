import React from "react";
import ReactDOM from "react-dom/client";

const Header = (props) => {
  return (
    <>
      <h1>{props.course}</h1>
    </>
  );
};

const Part = (props) => {
  return (
    <>
      <p>
        {props.name} {props.exercises}
      </p>
    </>
  );
};

const Content = (props) => {
  return (
    <div>
      <Part name={props.parts[0].name} exercises={props.parts[0].exercises} />
      <Part name={props.parts[1].name} exercises={props.parts[1].exercises} />
      <Part name={props.parts[2].name} exercises={props.parts[2].exercises} />
    </div>
  );
};

const Total = (props) => {
  const total = props.total.reduce(
    (previousValue, item) => previousValue + item.exercises,
    0
  );
  return (
    <>
      <p>
        Number of exercises {total}
      </p>
    </>
  );
};

const Course = (props) => {
  const { parts } = props.course;
  return (
    <>
      <Header course={props.course.name} />
      <Content parts={props.course.parts} />
      <Total total={parts} />
    </>
  );
};

const App = () => {
  const course = [
    {
      name:"Half Stack application development",
      id:1,
      parts:[
        {
          name:"Fundamentals of React",
          exercises:10,
          id:1,
        },
        {
          name:"Using props to pass data",
          exercises:7,
          id:2,
        },
        {
          name:"State of a component",
          exercises:14,
          id:3,
        },
        {
          name:"Redux",
          exercises:11,
          id:4,
        },       
      ]
    },
    {
      name:"Node.Js",
      id:2,
      parts:[
        {
          name:"Routing",
          exercises:3,
          id:1,
        },
        {
          name:"Middlewares",
          exercises:7,
          id:2,
        },
      ],
    },
  ]
  return <Course />;
};

const root = ReactDOM.createRoot(document.getElementById("root"));
root.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>
);
